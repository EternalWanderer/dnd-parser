package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
)

func main(){

	jsonFile, err := os.Open("ogthrak.json")
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Successfully opened char sheet.")
	}

	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	// Initialise the stats array
	var charInfo CharInfo

	json.Unmarshal(byteValue, &charInfo)

	fmt.Println("The currently loaded character is " +charInfo.Trivia.Name + " the " + charInfo.Trivia.SimpleClass + " who is level " + strconv.Itoa(charInfo.Trivia.Level))
	fmt.Println("He has a " + charInfo.Trivia.Background + " background and his alignment matches that of " + charInfo.Trivia.Alignment)
}
