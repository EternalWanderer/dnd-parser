package main

type CharInfo struct {
Statistics Statistics `json:"statistics"`
Trivia Trivia `json:"trivia"`
Saves Saves `json:"saving throws"`
Skills Skills `json:"skills"`
}

type Saves struct{
Strength  int `json:"strength"`
Dexterity int `json:"dexterity"`
Constitution int `json:"constitution"`
Intelligence int `json:"intelligence"`
Wisdom   int `json:"wisdom"`
Charisma int `json:"charisma"`
}

type Trivia struct{
Name string `json:"name"`
Class string `json:"class"`
SimpleClass string `json:"simple-class"`
Race string `json:"race"`
Background string `json:"background"`
Alignment string `json:"alignment"`
Level int `json:"level"`
}

type Statistics struct {
Strength int `json:"strength"`
Dexterity int `json:"dexterity"`
Constitution int `json:"constitution"`
Intelligence int `json:"intelligence"`
Wisdom int `json:"wisdom"`
Charisma int `json:"charisma"`
}

type Skills struct {
Acrobatics     int `json:"acrobatics"`
AnimalHandling int `json:"animal handling"`
Arcana         int `json:"arcana"`
Athletics      int `json:"athletics"`
Deception      int `json:"deception"`
History        int `json:"history"`
Insight        int `json:"insight"`
Intimidation   int `json:"intimidation"`
Medicine       int `json:"medicine"`
Nature         int `json:"nature"`
Perception     int `json:"perception"`
Performance    int `json:"performance"`
Persuasion     int `json:"persuasion"`
Religion       int `json:"religion"`
SleightOfHand  int `json:"sleight of hand"`
Stealth        int `json:"stealth"`
Survival       int `json:"survival"`
}
